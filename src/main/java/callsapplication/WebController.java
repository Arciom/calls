package callsapplication;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Spring's resource controller.
 */
@Controller
public class WebController extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    @GetMapping("/calls")
    public String showForm(CallsForm callsForm) {
        return "form";
    }

    @PostMapping("/calls")
    public String checkPersonInfo(@Valid CallsForm callsForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "form";
        }
        // make the file name like "LASTNAME_FIRSTNAME.txt"
        String fileName = callsForm.getLastName().toUpperCase() + "_" +
                          callsForm.getFirstName().toUpperCase() + ".txt";
        // write data to the file
        CallsUtils.writeCallToFile(fileName, callsForm);
        return "results";
    }
}


