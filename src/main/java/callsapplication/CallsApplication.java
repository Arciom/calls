package callsapplication;  

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Calls application class, makes this application executable.
 * Service is available at  http://localhost:8080/calls
 */
@SpringBootApplication
public class CallsApplication {

	/**
	 * Launches an application, using Spring Boot's SpringApplication.run().
	 */
	public static void main(String[] args) {
		SpringApplication.run(CallsApplication.class, args);
	}
}