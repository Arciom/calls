package callsapplication;

import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import callsconstraint.CheckBrackets;

/**
 * Call attributes representation class.
 * Has four fields:
 *  <ul>
 *  <li> firstName - max. length 30 characters. Optional. Any character.
 *  <li> lastName - max. length 30 characters. Mandatory. Any character.
 *  <li> time - not specified on input
 *  <li> telephoneNumber - mandatory attribute.
 *  </ul>
 */
public class CallsForm {

    @Size(max=30)
    private String firstName;

    @NotEmpty
    @Size(max=30)
    private String lastName;

    private String time;

    @NotEmpty
    // Validate the number - the number of start and end brackets must be the same.
    @CheckBrackets
    // Validate the number allowed number characters, -, (,), + and space.
    // The number is comprised of the local compulsory part of 9 numbers and the
    // optional international part of 5 numbers. The space, dash and bracket characters
    // can hold any position on input. The + character can only be at the beginning.
    @Pattern(regexp="^([+]?|(00)?)(([-() ]*\\d){3})?([-() ]*\\d){9}[-() ]*$",
             message ="Please enter correct number.")
    private String telephoneNumber;

    public CallsForm(){
    }

    public CallsForm(String firstName, String lastName, String time, String telephoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.time = time;
        this.telephoneNumber = telephoneNumber;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    @Override
    public String toString() {
        return "CallsForm{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", time='" + time + '\'' +
                ", telephoneNumber=" + telephoneNumber +
                '}';
    }
}

