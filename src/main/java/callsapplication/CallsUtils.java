package callsapplication;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *  Auxiliary class, with two methods.
 *   <ul>
 *   <li> normalizePhoneNumber() - normalizes the entered phone
 *                                number to the uniform format.
 *   <li> writeCallToFile() - writes call data to file.
 *   </ul>
 */
public class CallsUtils {

    /**
     * Normalizes the entered phone number to the uniform
     * format. 00YYY XXX XXX XXX
     * @param phoneNumber the phone number to be normalized.
     * @return phone number in normalized state.
     */
    public static String normalizePhoneNumber(String phoneNumber) {
        // leave only sign '+' at the beginning and digits in the phoneNumber
        String tempStr="";
        for (int i = 0, n = phoneNumber.length(); i < n; i++) {
            char c = phoneNumber.charAt(i);
            if ( c != '('& c !=')'& c!= ' ' & c !='-'){
                tempStr+=c;
            }
        }
        // change '+' to "00"
        if (tempStr.contains("+")){
            tempStr = tempStr.replace("+", "00");
        } else{
            // add code "00" if it isn't present
            if (!tempStr.substring(0, 2).equals("00")){
                tempStr = "00" + tempStr;
            }
        }
        StringBuilder str = new StringBuilder(tempStr);
        // if there are no any code, add the default code "420"
        if ((10 < tempStr.length()) & (tempStr.length() <14)){
            str.insert(2, "420");
        }
        str.insert(5, " ");
        str.insert(9, " ");
        str.insert(13, " ");
        return str.toString();
    }

    /**
     * Writes call data to a file.
     * @param fileName the name of file where this method saves data.
     * @param callsForm the instance of the CallsForm class to be saved to this file.
     * @throws IOException  if output exception occurred.
     */
    public static void writeCallToFile(String fileName, CallsForm callsForm) {
        File file = new File(fileName);
            try {
                if(!file.exists()){
                    file.createNewFile();
                }
                PrintWriter out = new PrintWriter(file.getAbsoluteFile());
                DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm:ss");
                try {
                    // print to the file, new line for every field
                    out.println(normalizePhoneNumber(callsForm.getTelephoneNumber()));
                    // the entry time of phone number
                    out.println(LocalDateTime.now().format(format));
                    out.println(callsForm.getFirstName());
                    out.println(callsForm.getLastName());
                    out.print(callsForm.getTime());
                } finally {
                    if(out != null) {
                        out.close();
                    }
                }
            } catch(IOException e) {
                throw new RuntimeException(e);
        }
    }
 }
