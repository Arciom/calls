package callsconstraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

/**
 *  Creates a custom constraint annotation.
 *  @see     Validation
 */
@Target(FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = CheckBracketsValidator.class)
@Documented
public @interface CheckBrackets {
    String message() default "The number of start and end brackets must be the same.";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
