package callsconstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Stack;

/**
 * Constraint Validator Class, performs the validation of
 * the same number of start and end brackets.
 */
public class CheckBracketsValidator implements ConstraintValidator<CheckBrackets, String> {

    @Override
    public void initialize(CheckBrackets constraintAnnotation) {
    }

    /**
     * Checks the number of start and end brackets.
     * @param number             the phone number to be checked.
     * @param constraintContext  a reference to an object that allows the validator
     *                           to push more information about the validation
     *                           failure to the implementor if it does not pass.
     * @return  true when the number of start and end brackets is the same,
     *          false otherwise.
     */
    @Override
    public boolean isValid(String number, ConstraintValidatorContext constraintContext) {
        Stack<Character> stack  = new Stack<Character>();
        for(int i = 0; i < number.length(); i++) {
            char c = number.charAt(i);
            if( c == '(' ) {
                stack.push(c);
            }else if(c == ')') {
                if(stack.isEmpty()) return false;
                if(stack.pop() != '(') return false;
            }
        }
        return stack.isEmpty();
    }
}
