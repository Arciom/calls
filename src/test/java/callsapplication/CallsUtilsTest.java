package callsapplication;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Test CallsUtils class methods.
 */
public class CallsUtilsTest {

    /**
     * Tests that normalized phone number not contains undeleted symbols.
     */
    @Test
    public void normalizePhoneNumberTest() {
        final String number = "+(333)-999(999)999";
        assertFalse(CallsUtils.normalizePhoneNumber(number).contains("+"));
        assertFalse(CallsUtils.normalizePhoneNumber(number).contains("("));
    }

    /**
     * Tests that writeCallToFile() method really writes data to file.
     * @throws IOException  if an input or output exception occurred.
     */
    @Test
    public void writeCallToFileTest() throws IOException {
        // Write call data to file
        CallsUtils.writeCallToFile("tempFile.txt", new CallsForm("First", "Second", "23:00", "123456789"));
        // Read all from temp file
        final String temp = Files.readAllLines(Paths.get("tempFile.txt")).toString();
        assertTrue(temp.contains("First"));
        assertTrue(temp.contains("Second"));
        assertTrue(temp.contains("23:00"));
        assertTrue(temp.contains(CallsUtils.normalizePhoneNumber("123456789")));
        Files.delete(Paths.get("tempFile.txt"));
    }
}